<?php
/**
 * Custom wp-config.php
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '{{DB_NAME}}');

/** MySQL database username */
define('DB_USER', '{{DB_USER}}');

/** MySQL database password */
define('DB_PASSWORD', '{{DB_PASSWORD}}');

/** MySQL hostname */
define('DB_HOST', '{{APP_NAME}}_db');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '6319b5c4a40fc07092cdc5fc5b1ed0dc32823c8e');
define('SECURE_AUTH_KEY',  '9804877ba2a707524eb697af79c139525eb032cc');
define('LOGGED_IN_KEY',    '86922c3d3356b0d1781adcf5b68dcb4ffdb69991');
define('NONCE_KEY',        '893a6d5987073d30404537b4946954c2e7f0cdd0');
define('AUTH_SALT',        '0cc38e6e53990111d8a17b3e41cf87c47535de9f');
define('SECURE_AUTH_SALT', 'beef78823ec9249a3d6587d090917156ec7cf0c6');
define('LOGGED_IN_SALT',   '3e9a5b9f766252e777a714513f8d9d970d8fdbbe');
define('NONCE_SALT',       '91c86b8cce1f7f93142cd68fed0b48a26dcf6b67');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Desativa edição de arquivos */
define( 'DISALLOW_FILE_EDIT', {{DISALLOW_FILE_EDIT}} );

// If we're behind a proxy server and using HTTPS, we need to alert Wordpress of that fact
// see also http://codex.wordpress.org/Administration_Over_SSL#Using_a_Reverse_Proxy
if (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https') {
	$_SERVER['HTTPS'] = 'on';
}

/* multisite */
define('WP_ALLOW_MULTISITE', true);
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', '{{MAIN_DOMAIN}}');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

@ini_set( 'upload_max_size' , '256M' );
@ini_set( 'post_max_size', '256M');
@ini_set( 'max_execution_time', '300' );

define('FS_METHOD','direct');


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
