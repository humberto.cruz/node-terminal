FROM wordpress:{{WP_VERSION}}
# Instala o certificado de rede da Poupex
COPY ./conf/root.cert.crt /usr/local/share/ca-certificates/root.cert.crt

# Copia arquivos de configuração do servidor WEB e PHP
COPY ./conf/.htaccess /usr/src/wordpress/.htaccess
COPY ./conf/php.ini /usr/src/wordpress/php.ini
COPY ./conf/uploads.ini /usr/local/etc/php/conf.d/uploads.ini
COPY ./conf/apache-default.conf /etc/apache2/sites-enabled/default.conf
COPY ./conf/wp-config.php /usr/src/wordpress/wp-config.php

# Configura permissões dos arquivos do WP
RUN chown -R www-data:www-data /usr/src/wordpress
RUN find /usr/src/wordpress/ -type d -print0|xargs -0 chmod 775
RUN find /usr/src/wordpress/ -type f -print0|xargs -0 chmod 664

# Instala pacotes necessários
RUN update-ca-certificates \
	&& echo "search poupex.com.br" >> /etc/resolv.conf \
	# Remove plugins, themas e uploads do wordpress original
	&& rm -rf /usr/src/wordpress/wp-content/plugins/* \
	&& rm -rf /usr/src/wordpress/wp-content/themes/* \
	&& rm -rf /usr/src/wordpress/wp-content/uploads/* \
	# Prepara o APT para instalar alguns pacotes
	&& apt-get update \
	# Instala alguns pacotes
	&& apt-get install -y sudo nano less mariadb-client pdftk \
	# Remove a lista de sources para economizar espaço
	&& rm -rf /var/lib/apt/lists/* \
	# Remove automaticamente pacotes não necessários
	&& apt-get purge -y --auto-remove

# Instala o WP-CLI
RUN curl -o /bin/wp https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
RUN chmod +x /bin/wp
