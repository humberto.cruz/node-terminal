const isRoot = require('is-root');
class WPexApp {
  constructor(){
    if (isRoot()) {
      // Variaveis preenchidas pelos selects ( menus )
      this.vars = {
        comm:{text:' ---'},
        site:{text:' ---'},
        env:{text:' ---'}
      };
      this.username = '';
      this.password = '';
      // Git
      this.git = require('simple-git/promise');
      // Caminho de arquivos
      this.path = require('path');
      // Cria diretorios
      this.mkdirp = require('mkdirp');
      // Filesystem
      this.fs = require('fs');
      // Remove não vazios
      this.rimraf = require('rimraf');
      // Para executar arquivos shell
      this.shell = require('shelljs');
      // Log coloridos
      this.chalk = require('chalk');
      // Limpar a tela
      this.clearScreen = require('clear');
      // Espera (x) segundos
      this.sleep = require('sleep');
      // Config
      this.config = JSON.parse(this.fs.readFileSync('./config.json','utf8'));
      
      // Bind this nas seguintes funções:
      this.colorText = this.colorText.bind(this);
      this.log - this.log.bind(this);
      this.choiceSite = this.choiceSite.bind(this);
      this.choiceEnv = this.choiceEnv.bind(this);
      this.choiceCommand = this.choiceCommand.bind(this);
      this.processCommand = this.processCommand.bind(this);
      this.showHeader = this.showHeader.bind(this);
      this.createSiteFolders = this.createSiteFolders.bind(this);
      this.fullLines = this.fullLines.bind(this);
      this.dbComm = this.dbComm.bind(this);
      
      // Modulos
      this.clearComm = this.clearComm.bind(this);
      this.prepareComm = this.prepareComm.bind(this);
      this.checkConfig();
    } else {
      // Log coloridos
      this.chalk = require('chalk');
      // Limpar a tela
      this.clearScreen = require('clear');
      // Bind this nas seguintes funções:
      this.colorText = this.colorText.bind(this);
      this.log - this.log.bind(this);
      
      this.noRootError();
    }
  }
  // Verifica configurações
  checkConfig(){
    this.choiceSite();
  }
  // Substitui options
  replaceConfig(content) {
    const configFile = this.config;
    const configs = configFile[this.vars['site'].value][this.vars['env'].value];
    let text = content;
    for (var config in configs){
      const search = new RegExp('{{'+config+'}}','g');
      text = text.replace(search,configs[config]);
    }
    return text;
  }
  // Não executando como root ( sudo )
  noRootError(){
    this.log('É necessário executar esse script como root ou usar o "sudo"...','red');
    process.exit(0);
  }
  // Opções padrão para os selects ( menu )
  selectOptions(){
    return {
      msgCancel: 'Operação cancelada!',
      pointer: ' ▸ ',
      pointerColor: 'green',
      multiSelect: false,
      inverse: true,
      prepend: false,
      disableInput:true,
    }
  }
  // Log colorido just color
  colorText(text,color='white'){
    return this.chalk[color](text);
  }
  // Log colorido
  log(text,color='white'){
    process.stdout.write(this.colorText(text,color)+'\n');
    //console.log(this.colorText(text,color));
  }
  // Adiciona opções aos menu
  // menu: menu sendo construido
  // options: array de opções
  // {
  //  text: nome que aparece no menu
  //  value: valor a ser passado
  //}
  async createSiteFolders(){
    const directory = this.path.join('.','sites',this.vars['site'].value);
    this.log('Aguardando criação dos diretórios...','yellow');
    await this.mkdirp(directory);
    await this.mkdirp(this.path.join(directory,'conf'));
    await this.mkdirp(this.path.join(directory,'db'));
    await this.mkdirp(this.path.join(directory,'logs'));
    await this.mkdirp(this.path.join(directory,'uploads'));
    this.log('Diretórios criados...','green');
  }
  addOptions(menu, options){
    options.map(o=>menu.option(o.text,o.value))
    menu.list();
  }
  fullLines(c,color='white'){
    let line = '';
    while (line.length < process.stdout.columns){
      line+=c;
    }
    return this.colorText(line,color);
  }
  showHeader(){
    this.log(this.fullLines('=','blue'));
    this.log('Site:      '+this.vars['site'].text,'green');
    this.log('Ambiente:  '+this.vars['env'].text,'yellow');
    this.log(this.fullLines('-','blue'));
  }
  // Menu para escolher o site a ser montado
  async choiceSite(){
    this.clearScreen();
    this.log('');
    this.showHeader();
    this.log('');
    this.log(' Escolha o site que deseja utilizar:  ','green');
    this.log('');
    const menu = require('select-shell')(this.selectOptions());
    this.addOptions(menu,[
      {text:' FHE        ',value:'fhe'},
      {text:' POUPEX     ',value:'poupex'},
    ]);
    await menu.on('select', function(options){
      this.vars['site'] = options[0];
      this.choiceEnv();
    }.bind(this));
  }
  // Menu para escolher o ambiente a ser montado
  choiceEnv(){
    this.clearScreen();
    this.log('');
    this.showHeader();
    this.log('');
    this.log(' Escolha o ambiente que deseja utilizar: ','green');
    this.log('');
    const menu = require('select-shell')(this.selectOptions());
    this.addOptions(menu,[
      {text:' Desenvolvimento ',value:'dev'},
      {text:' Homologação     ',value:'hml'},
      {text:' Produção        ',value:'prod'},
    ]);
    menu.on('select', function(options){
      this.vars['env'] = options[0];
      this.choiceCommand();
    }.bind(this));
  }
  // Menu para escolher o comando a ser executado
  choiceCommand(){
    this.clearScreen();
    this.log('');
    this.showHeader();
    this.log('');
    this.log(' Escolha o comando que deseja executar: ','green');
    this.log('');
    const menu = require('select-shell')(this.selectOptions());
    this.addOptions(menu,[
      {text:' Configurar Ambiente        ',value:'prepare'},
      {text:' Limpar Ambiente            ',value:'clear'},
      {text:' Download do Banco de Dados ',value:'gitdb'},
      {text:' Manter Themes/Plugins      ',value:'wp-update'},
      {text:' Docker                     ',value:'docker'},
      {text:' -------------------------- ',value:'---'},
      {text:' Alterar Site               ',value:'back'},
      {text:' -------------------------- ',value:'---'},
      {text:' Sair                       ',value:'exit'},
    ]);
    menu.on('select', function(options){
      if (options[0].value=='db') this.choiceBd();
      else { 
        this.vars['comm'] = options[0];
        this.processCommand();
      }
    }.bind(this));
  }
  // Menu para Ferramentas de Banco
  choiceBd(){
    this.clearScreen();
    this.log('');
    this.showHeader();
    this.log('');
    this.log(' Escolha o que deseja realizar no banco de dados: ','green');
    this.log('');
    const menu = require('select-shell')(this.selectOptions());
    this.addOptions(menu,[
      {text:this.colorText(' Substituir dominio          ','green'),value:'dom'},
      {text:this.colorText(' Download do repositório GIT ','yellow'),value:'gitdb'},
      {text:this.colorText(' ----------------------- ','white'),value:'---'},
      {text:this.colorText(' Alterar Site            ','magenta'),value:'back'},
      
    ]);
    menu.on('select', function(options){
      this.vars['comm'] = options[0];
      this.processCommand();
    }.bind(this));
  }
  // Process arquivos bash de acordo com o comando escolhido
  // Todo: transformar os arquivos bash em node
  async processCommand(){
    const comm = this.vars['comm'].value;
    if (comm=='clear') {
      this.log('Limpando Ambiente','yellow');
      await this.clearComm();
      this.choiceCommand();
    }
    if (comm=='gitdb'){
      this.log('Baixando Banco de Dados...','yellow');
      await this.dbComm();
      this.choiceCommand();
    }
    if (comm=='prepare'){
      this.log('Criando Ambiente','blue');
      this.prepareComm();
      this.choiceCommand();
    }
    /*
    switch(this.vars['comm'].value) {
      case 'clear':
        this.log('Limpar Ambiente','blue');
        await this.clearComm();
        this.log('Ambiente limpo com sucesso!','green');
        await this.sleep.sleep(3);
        this.choiceCommand();
        break;
      case 'prepare':
        
      case 'gitdb':
        this.log('Download do Banco de Dados.','blue');
        await this.dbComm();
        await this.sleep.sleep(3);
        this.choiceCommand();
        break;
      case 'docker':
        this.log('Tarefas do Docker.','blue');
        await this.dockerComm();
        this.choiceCommand();
        break;
      case 'wp-update':
        this.log('Tarefas do Wordpress.','blue');
        await this.wpComm();
        this.choiceCommand();
        break;
      case 'exit':
        this.log('Saindo...','blue');
        await this.sleep.sleep(3);
        process.exit(0);
        break;
      case 'back':
        this.choiceSite();
        break;
      case 'build':
        this.buildDocker();
        break;
      case 'start':
        this.startDocker();
        break;
      default:
        this.log('Comando não implementado!','red');
        this.choiceCommand();
        break;
    }
    */
  }
  async clearComm(){
    const directory = this.path.join('.','sites',this.vars['site'].value);
    await this.rimraf(directory,function(err){
      if(err)console.log(err);
    });
  }
  async prepareComm(){
    let promisses = [];
    promisses.push(this.createSiteFolders());
    const directory = this.path.join('.','sites',this.vars['site'].value);
    promisses.push(
      this.fs.readdir(this.path.join('.','src','conf'),async function(err,files){
        files.forEach(async function(file){
          let content = await this.fs.readFileSync(this.path.join('.','src','conf',file));
          content = this.replaceConfig(content.toString());
          let savePath = '';
          if (file=='docker-compose.yml'||file=='docker-compose.override.yml'||file=='Dockerfile'){
            savePath = this.path.join(directory,file);
          } else {
            savePath = this.path.join(directory,'conf',file);
          }
          if (this.vars['env']!='prod'||file!='docker-compose.override.yml') {
            await this.fs.writeFile(savePath, content, function(err){
              if(err) console.log(err);
            });
          }
        }.bind(this));
      }.bind(this))
    );
    await Promise.all(promisses);
    promisses.push(this.log('Ambiente limpo com sucesso!','green'));
    promisses.push(this.sleep.sleep(1));
    this.choiceCommand();
  }
  async dockerComm(){

  }
  async dbComm(){
    let repoDir = this.path.join('.', this.vars['site'].value+'-db');
    const configFile = this.config;
    const gitUrl = 'https://'+configFile.GIT_USER+':'+configFile.GIT_PASS+'@'+'gitlab.poupex.com.br/poupex/projetos/wordpress/'+repoDir;

    let promisses = [];
    promisses.push(this.rimraf(repoDir,function(){}));
    await Promise.all(promisses);
    
    repoDir = this.path.join('.', this.vars['site'].value+'-db.git');
    promisses = [];
    promisses.push(this.git().clone(gitUrl));
    await Promise.all(promisses);
    
    promisses = [];
    promisses.push(this.log('Banco de dados baixado com sucesso!','green'));
    promisses.push(this.sleep.sleep(1));
    await Promise.all(promisses);
    this.choiceCommand();
  }
  async wpComm(){

  }
}
module.exports = WPexApp